﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACCI
{
    class Maquina
    {
        // Maquina de laboratorios de practica
        private int numeromaquina;

        public int NumeroMaquina
        {
            get { return numeromaquina; }
            set { numeromaquina = value; }
        }
        // maquina de docentes o de areas administrativas 
        private string  encargado;

        public string  Encargado
        {
            get { return encargado; }
            set { encargado = value; }
        }

    }
}

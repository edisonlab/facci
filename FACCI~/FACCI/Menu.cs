﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACCI
{
    class Menu
    {
        Administrador administrador = new Administrador();
        Docente docente = new Docente();
        Estudiante estudiante = new Estudiante();
        Registro registro = new Registro();
        Login login = new Login();
        int opcion = 0;
        public void IngresarComo()
        {
            Console.WriteLine("Menu");
            Console.WriteLine("1. Administrador");
            Console.WriteLine("2. Estudiante");
            Console.WriteLine("3. Docente");
            Console.WriteLine("Ingrese como "); opcion = Convert.ToInt32(Console.ReadLine());

            if (opcion == 1)
            {
                login.IniciarSesion();
                administrador.RegistroAdmni();
                // Lista de Incidencias
            }
            else
            {
                if (opcion == 2)
                {
                    login.IniciarSesion();
                    estudiante.RegistroEstudiante();
                    registro.RegistroIncidenciaEstudiante();
                    login.CerrarSesion();
                }
                else
                {
                    if (opcion == 3)
                    {
                        login.IniciarSesion();
                        docente.RegistroDocente();
                        registro.RegistroIncidenciaDocente();
                        login.CerrarSesion();
                    }
                    else
                    {
                        
                    }
                }
            }
        }

    }
}

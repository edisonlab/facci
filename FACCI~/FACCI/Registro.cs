﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACCI
{
    class Registro
    {
        private DateTime fecha = DateTime.Today;

        public DateTime Fecha
        {
            get { return fecha; }
            set { fecha = value; }
        }
        private string tipoincidencia;

        public string TipoIncidencia
        {
            get { return tipoincidencia; }
            set { tipoincidencia = value; }
        }
        private string comentario;

        public string Comentario
        {
            get { return comentario; }
            set { comentario = value; }
        }
        private Laboratorio laboratorio;


        public Laboratorio Laboratorio
        {
            get { return laboratorio; }
            set { laboratorio = value; }
        }
        private Maquina computador;

        public Maquina Computador
        {
            get { return computador; }
            set { computador = value; }
        }
        private Login usuario;
        public Login Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }

        public void RegistroIncidenciaDocente()
        {
            Console.WriteLine("------------REGISTRO-----------");
            Console.WriteLine("Fecha: "+fecha);
            Console.WriteLine("Laboratorio o área Administrativa: ");laboratorio.Descripcion = Console.ReadLine();
            Console.WriteLine("Encargado de Máquina : ");computador.Encargado = Console.ReadLine();
            Console.WriteLine("Usuario:"+usuario.CorreoInstitucional);
            Console.WriteLine("Tipo de Incidencia");tipoincidencia = Console.ReadLine();
            Console.WriteLine("Comentario");comentario = Console.ReadLine();
        }
        public void RegistroIncidenciaEstudiante()
        {
            Console.WriteLine("------------REGISTRO-----------");
            Console.WriteLine("Fecha: " + fecha);
            Console.WriteLine("Laboratorio : "); laboratorio.NumeroLaboratorio = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Número de Máquina : "); computador.NumeroMaquina =Convert.ToInt32( Console.ReadLine());
            Console.WriteLine("Usuario:" + usuario.CorreoInstitucional);
            Console.WriteLine("Tipo de Incidencia"); tipoincidencia = Console.ReadLine();
            Console.WriteLine("Comentario"); comentario = Console.ReadLine();
        }
    }
}

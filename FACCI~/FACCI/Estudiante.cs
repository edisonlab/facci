﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACCI
{
    class Estudiante
    {
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        private string apellidos;

        public string Apellidos
        {
            get { return apellidos; }
            set { apellidos = value; }
        }
        private string cedula;

        public string Cedula
        {
            get { return cedula; }
            set { cedula = value; }
        }

        private string nivel;

        public string Nivel
        {
            get { return nivel; }
            set { nivel = value; }
        }

        public void RegistroEstudiante()
        {
            Console.Write("Nombres: "); nombre = Console.ReadLine();
            Console.Write("Apellidos: "); apellidos = Console.ReadLine();
            Console.WriteLine("N° Identidad"); cedula = Console.ReadLine();
            Console.Write("Nivel: "); nivel = Console.ReadLine();
        }
      
    }
}
